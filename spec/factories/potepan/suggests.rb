FactoryBot.define do
  factory :potepan_suggest, class: 'Potepan::Suggest' do
    sequence(:keyword) { |n| "ruby#{n}" }
  end
end
