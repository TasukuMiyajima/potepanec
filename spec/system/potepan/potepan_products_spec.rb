require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  describe "products/show" do
    context "when product has taxons" do
      let(:taxon) { create(:taxon) }
      let(:product) { create(:product, taxons: [taxon]) }

      before do
        visit potepan_product_path(product.id)
      end

      it "displays correctly" do
        expect(page).to have_title "#{product.name} - BIGBAG Store"
        expect(page).to have_selector '.media-body', text: "#{product.name}"
        expect(page).to have_selector '.media-body', text: "#{product.display_price}"
        expect(page).to have_selector '.media-body', text: "#{product.description}"
        link = find('.potepan-link', text: "Home")
        expect(link[:href]).to eq potepan_path
      end

      it "has link for potepan/categories/show" do
        expect(page).to have_content "一覧ページへ戻る"
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "when product doesn't have taxons" do
      let!(:root_taxon) { create(:taxon, id: 1) }
      let(:no_taxon_product) { create(:product) }

      it "has link for potepan/categories/show" do
        visit potepan_product_path(no_taxon_product.id)
        expect(page).to have_content "一覧ページへ戻る"
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(1)
      end
    end

    context "when product has one related product" do
      let(:taxon) { create(:taxon) }
      let(:product) { create(:product, taxons: [taxon]) }
      let!(:related_product) { create(:product, taxons: [taxon]) }

      before do
        visit potepan_product_path(product.id)
      end

      it "displays related_products" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        click_link related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end

    context "when product has five related products" do
      let(:taxon) { create(:taxon) }
      let(:product) { create(:product, taxons: [taxon]) }
      let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

      before do
        visit potepan_product_path(product.id)
      end

      it "displays related_products" do
        expect(page).to have_content product.name, count: 3
        expect(page).to have_content related_products.first.name, count: 1
        expect(page).to have_content related_products.second.name, count: 1
        expect(page).to have_content related_products.third.name, count: 1
        expect(page).to have_content related_products.fourth.name, count: 1
        expect(page).not_to have_content related_products.fifth.name
        click_link related_products.first.name
        expect(current_path).to eq potepan_product_path(related_products.first.id)
      end
    end
  end
end
