require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  describe "categories/show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
    let(:params) { { name: "Ruby on Rails 2", parent: taxonomy.root, taxonomy: taxonomy } }
    let(:other_taxon) { create(:taxon, params) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon], price: 20) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "displays correctly" do
      expect(page).to have_title "#{taxon.name} - BIGBAG Store"
      expect(page).to have_selector '.page-title', text: "#{taxon.name}"
      link = find('.potepan-link', text: "Home")
      expect(link[:href]).to eq potepan_path
    end

    it "has link for potepan/categories/show" do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        click_link taxonomy.name
        expect(page).to have_link taxon.name
        expect(page).to have_content "(#{taxon.products.length})"
        expect(page).to have_link other_taxon.name
        expect(page).to have_content "(#{other_taxon.products.length})"
        click_link other_taxon.name
      end
      expect(current_path).to eq potepan_category_path(other_taxon.id)
    end

    it "has link for potepan/products/show" do
      within '.productBox' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_no_content other_product.name
        expect(page).to have_no_content other_product.display_price
        click_link product.name
      end
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
