require 'rails_helper'

RSpec.describe Potepan::ProductDecorator do
  describe "related_products" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    context "when product has no related product" do
      it "is empty" do
        expect(product.related_products).to be_empty
      end
    end

    context "when product has one related product" do
      let!(:related_product) { create(:product, taxons: [taxon]) }

      it "returns one related_product" do
        expect(product.related_products).to match_array related_product
      end
    end

    context "when product has five related products" do
      let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

      it "returns 4 related_products" do
        expect(product.related_products).to eq related_products[0..4]
      end
    end
  end
end
