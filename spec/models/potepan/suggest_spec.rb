require 'rails_helper'

RSpec.describe Potepan::Suggest, type: :model do
  describe "Potepan::Suggest" do
    context "with keyword" do
      let!(:suggest) { create(:potepan_suggest, keyword: "ruby1") }
      let(:duplicated_suggest) { build(:potepan_suggest, keyword: "ruby1") }

      it "is valid" do
        expect(suggest).to be_valid
      end

      it "returns a keyword as a string" do
        expect(suggest.keyword).to eq "ruby1"
      end

      it "is invalid with a duplicate keyword" do
        duplicated_suggest.valid?
        expect(duplicated_suggest.errors[:keyword]).to eq ["has already been taken"]
      end
    end

    context "without keyword" do
      let(:suggest) { build(:potepan_suggest, keyword: nil) }

      it "is invalid" do
        suggest.valid?
        expect(suggest.errors[:keyword]).to eq ["can't be blank"]
      end
    end
  end

  describe "self.search" do
    let!(:suggest1) { create(:potepan_suggest, keyword: "ruby1") }
    let!(:suggest2) { create(:potepan_suggest, keyword: "ruby2") }
    let!(:suggest3) { create(:potepan_suggest, keyword: "ruby3") }

    context "when keyword is initial word" do
      context "when max_num is one" do
        it "returns one suggested word" do
          expect(Potepan::Suggest.search("r", 1).pluck(:keyword)).to eq ["ruby1"]
        end
      end

      context "when max_num is five" do
        it "returns max suggested words" do
          expect(Potepan::Suggest.search("r", 5).pluck(:keyword)).to eq ["ruby1", "ruby2", "ruby3"]
        end
      end
    end

    context "when keyword is not initial word" do
      it "returns no suggested word" do
        expect(Potepan::Suggest.search("u", 1).pluck(:keyword)).to eq []
      end
    end
  end
end
