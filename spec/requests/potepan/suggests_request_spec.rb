require 'rails_helper'

RSpec.describe "Potepan::Suggests", type: :request do
  describe "GET /search" do
    WebMock.enable!
    let(:api_uri) { Rails.application.credentials.suggest_api[:api_uri] }
    let(:api_key) { Rails.application.credentials.suggest_api[:api_key] }

    context "with correct keyword" do
      before do
        stub_request(:get, api_uri).
          with(
            headers: { Authorization: "Bearer #{api_key}" },
            body: { keyword: "r", max_num: 5 }
          ).
          to_return(
            body: ["ruby10", "ruby20", "ruby30", "ruby40", "ruby50"].to_json,
            status: 200
          )
        get potepan_suggest_path, params: { keyword: "r", max_num: 5 }
      end

      it "returns http success" do
        expect(response.status).to eq 200
      end

      it "responds with JSON formatted output" do
        expect(response.content_type).to eq "application/json"
      end

      it "responds correct instance variable" do
        expect(JSON.parse(response.body)).to eq ["ruby10", "ruby20", "ruby30", "ruby40", "ruby50"]
      end
    end

    context "with empty keyword" do
      before do
        get potepan_suggest_path, params: { keyword: "", max_num: 5 }
      end

      it "returns http status" do
        expect(response.status).to eq 400
      end

      it "returns error message" do
        expect(response.body).to eq "No Keyword"
      end
    end
  end
end
