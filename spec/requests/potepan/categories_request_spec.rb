require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy, name: "Ruby Brand") }
    let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy, name: "Rails") }
    let!(:product) { create(:product, taxons: [taxon], name: "Bag") }

    before do
      get potepan_category_path(taxon.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns http success" do
      expect(response).to have_http_status(200)
    end

    it "displays correct instance variable" do
      expect(response.body).to include "Ruby Brand"
      expect(response.body).to include "Rails"
      expect(response.body).to include "Bag"
    end
  end
end
