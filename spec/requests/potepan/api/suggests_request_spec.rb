require 'rails_helper'

RSpec.describe "Potepan::Api::Suggests", type: :request do
  describe "GET /show" do
    let!(:suggests) { create_list(:potepan_suggest, 101) }
    let(:token) { Rails.application.credentials.authenticate_token[:token] }
    let(:params) { { keyword: keyword, max_num: max_num } }

    context "without exception error" do
      before do
        get potepan_api_suggests_path,
            headers: headers,
            params: params
      end

      context "with correct token" do
        let(:headers) { { Authorization: "Bearer #{token}" } }

        context "when keyword is present" do
          shared_examples "http check" do
            it "returns http success" do
              expect(response.status).to eq 200
            end
          end

          shared_examples "output format check" do
            it "responds with JSON formatted output" do
              expect(response.content_type).to eq "application/json"
            end
          end

          shared_examples "default keyword numbers check" do
            it "returns 10 keywords" do
              expect(JSON.parse(response.body)).to eq suggests[0..9].pluck(:keyword)
            end
          end

          context "when keyword can suggest" do
            let(:keyword) { "r" }

            context "when max_num is positive and less than 10" do
              let(:max_num) { 5 }

              it_behaves_like "http check"
              it_behaves_like "output format check"

              it "returns keywords which numbers are limited by max_num" do
                expect(JSON.parse(response.body)).to eq suggests[0..4].pluck(:keyword)
              end
            end

            context "when max_num is over 100" do
              let(:max_num) { 101 }

              it_behaves_like "http check"
              it_behaves_like "output format check"

              it "returns 100 keywords" do
                expect(JSON.parse(response.body)).to eq suggests[0..99].pluck(:keyword)
              end
            end

            context "when max_num is negative number" do
              let(:max_num) { -5 }

              it_behaves_like "http check"
              it_behaves_like "output format check"
              it_behaves_like "default keyword numbers check"
            end

            context "when max_num is String" do
              let(:max_num) { "r" }

              it_behaves_like "http check"
              it_behaves_like "output format check"
              it_behaves_like "default keyword numbers check"
            end

            context "when max_num is empty" do
              let(:max_num) { "" }

              it_behaves_like "http check"
              it_behaves_like "output format check"
              it_behaves_like "default keyword numbers check"
            end

            context "when max_num is nil" do
              let(:max_num) { nil }

              it_behaves_like "http check"
              it_behaves_like "output format check"
              it_behaves_like "default keyword numbers check"
            end
          end

          context "when keyword cannot suggest" do
            let(:params) { { keyword: "1", max_num: 5 } }

            it_behaves_like "http check"
            it_behaves_like "output format check"

            it "returns no keyword" do
              expect(response.body).to eq "[]"
            end
          end
        end

        context "when keyword is blank" do
          let(:max_num) { 5 }

          shared_examples "http error check" do
            it "returns http status" do
              expect(response.status).to eq 400
            end
          end

          shared_examples "body check" do
            it "responds error message" do
              expect(response.body).to eq "'keyword' can't be blank"
            end
          end

          context "when keyword is empty" do
            let(:keyword) { "" }

            it_behaves_like "http error check"
            it_behaves_like "body check"
          end

          context "when keyword is nil" do
            let(:keyword) { nil }

            it_behaves_like "http error check"
            it_behaves_like "body check"
          end
        end
      end

      context "without correct token" do
        let(:params) { { keyword: "r", max_num: 5 } }

        shared_examples "http check" do
          it "returns http status" do
            expect(response).to have_http_status 401
          end
        end

        shared_examples "message check" do
          it "returns error message" do
            expect(response.body).to eq "unauthorized"
          end
        end

        context "with no token" do
          let(:headers) { nil }

          it_behaves_like "http check"
          it_behaves_like "message check"
        end

        context "with wrong token" do
          let(:headers) { { Authorization: "Bearer 111" } }

          it_behaves_like "http check"
          it_behaves_like "message check"
        end
      end
    end

    context "when exception error happens" do
      let(:headers) { { Authorization: "Bearer #{token}" } }
      let(:params) { { keyword: "r", max_num: 5 } }

      before do
        allow(Potepan::Suggest).to receive(:search).and_raise(RuntimeError)
        get potepan_api_suggests_path,
            headers: headers,
            params: params
      end

      it "returns http status" do
        expect(response).to have_http_status 500
      end

      it "returns error message" do
        expect(response.body).to eq "RuntimeError"
      end
    end
  end
end
