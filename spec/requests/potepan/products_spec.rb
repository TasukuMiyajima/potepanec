require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon], name: "Bag") }
    let!(:related_product) { create(:product, taxons: [taxon], name: "tote") }

    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns http success" do
      expect(response).to have_http_status(200)
    end

    it "has correct instance variable" do
      expect(response.body).to include "Bag"
      expect(response.body).to include "tote"
    end
  end
end
