require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "without page title" do
      it "returns only base title" do
        expect(full_title("")).to eq("BIGBAG Store")
        expect(full_title(nil)).to eq("BIGBAG Store")
      end
    end

    context "with page title" do
      it "returns full title" do
        expect(full_title("Ruby")).to eq("Ruby - BIGBAG Store")
      end
    end
  end
end
