module Constants
  RELATED_PRODUCTS_MAX_COUNT = 4
  SUGGESTED_KEYWORDS_MAX_COUNT = 100
  SUGGESTED_KEYWORDS_DEFAULT_COUNT = 10
end
