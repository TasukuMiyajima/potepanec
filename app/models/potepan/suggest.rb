class Potepan::Suggest < ApplicationRecord
  validates :keyword, presence: true, uniqueness: true

  def self.search(keyword, max_num)
    Potepan::Suggest.where(['keyword LIKE ?', "#{keyword}%"]).limit(max_num)
  end
end
