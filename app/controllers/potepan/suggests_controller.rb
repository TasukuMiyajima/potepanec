require 'api_suggest'

class Potepan::SuggestsController < ApplicationController
  def search
    if params[:keyword].present?
      suggests = ApiSuggest.suggest(params[:keyword], params[:max_num])
      if suggests.status == 200
        render json: JSON.parse(suggests.body)
      else
        render json: suggests.body, status: suggests.status
      end
    else
      render json: "No Keyword", status: 400
    end
  end
end
