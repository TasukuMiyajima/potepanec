class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.available.find(params[:id])
    @related_products = @product.related_products.limit(Constants::RELATED_PRODUCTS_MAX_COUNT)
  end
end
