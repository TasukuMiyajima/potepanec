class Potepan::Api::SuggestsController < ApplicationController
  before_action :authenticate

  def index
    return render json: "'keyword' can't be blank", status: 400 if suggest_params[:keyword].blank?
    max_num = if suggest_params[:max_num].to_i.positive?
                [suggest_params[:max_num].to_i, Constants::SUGGESTED_KEYWORDS_MAX_COUNT].min
              else
                Constants::SUGGESTED_KEYWORDS_DEFAULT_COUNT
              end
    response = Potepan::Suggest.search(suggest_params[:keyword], max_num).pluck(:keyword)
    render json: response, status: 200
  rescue => e
    render json: e.message, status: 500
    logger.error e
    logger.error e.backtrace.join("\n")
  end

  private

  def suggest_params
    params.permit(:keyword, :max_num)
  end

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token|
      token == Rails.application.credentials.authenticate_token[:token]
    end
  end

  def render_unauthorized
    render json: "unauthorized", status: 401
  end
end
