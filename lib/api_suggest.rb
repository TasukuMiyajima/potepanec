require 'httpclient'
require 'json'

class ApiSuggest
  API_URI = Rails.application.credentials.suggest_api[:api_uri]
  API_KEY = Rails.application.credentials.suggest_api[:api_key]

  def self.suggest(keyword, max_num)
    headers = {
      Authorization: "Bearer #{API_KEY}",
    }
    params = {
      keyword: keyword,
      max_num: max_num,
    }
    client = HTTPClient.new
    client.get(API_URI, header: headers, body: params)
  end
end
